# nginx_redirect_module
This is a Nginx module to do url redirction based on the provided csv file. The csv file structure should be like:

old url | new url |  redirect code


1. Install
	You need to get nginx build version first.
	Then copy the folder ngx_http_dunelm_redirect_module.
	In the Nginx folder run sudo ./configure ... --add-module= path/to/the/ngx_http_dunelm_redirect_module/folder
	Then run sudo make && sudo make install

2. Configure
	Two directives have been created to do the url btree construction and the url redirection, Here is my configuration for local test:

	server {
	        listen       80;
	        server_name  localhost;

	        #charset koi8-r;

	        #access_log  logs/host.access.log  main;

	        
	        
	        location ~* (?!/testtq) {
	            dunelm_redirect on;
	        }

	        location = /testtq {
	            dunelm_btree /path/to/the/csv/file 
	        }
	}

	To enable the module, 
	hostname/testtq has to be visit first to load the csv file and construct btree in Nginx pool or Internal Error will be sent.

	For directive dunelm_btree, there has to be one augument to specify the path to the csv file.
	For directive dunelm_redirect, there has to be on augument 'on' the enable the redirect action.
	You can also use the redirect on particular domain:

	location ~* ^(https?:\/\/(w{3}\.)?cagesworld.co.uk[\s\S]*?$ {
	            dunelm_redirect on;
	        }