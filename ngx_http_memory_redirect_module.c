#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include <string.h>
#include <sys/stat.h>

#define HELLO_WORLD "create btree in pool"


typedef struct {
    ngx_flag_t enabled;
    ngx_str_t file_path;
    //ngx_str_t file_exist;
} ngx_http_memory_redirect_loc_conf_t;
/*
typedef struct {
	ngx_str_t file_path;
} ngx_http_memory_btree_loc_conf_t;
*/

typedef struct ngx_http_memory_redirect_address_node{ //address node to store the old and redirect url from csv file
	char old_address[1500];
	char new_address[180];
	char redirect_code[10];

} ngx_http_memory_redirect_address_node;

typedef struct ngx_http_memory_redirect_bst_node_address_info{
    char new_address[180];
    char redirect_code[10];
} ngx_http_memory_redirect_bst_node_address_info;

typedef struct ngx_http_memory_redirect_bst_node{   //b-tree node
	char index[1500];
    ngx_http_memory_redirect_bst_node_address_info address_info;
	struct ngx_http_memory_redirect_bst_node *lchild, *rchild;
} ngx_http_memory_redirect_bst_node;

static char *ngx_http_memory_btree(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
//static char *ngx_http_memory_redirect(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static int cfileexists(const char* filename);
static ngx_int_t ngx_http_memory_btree_handler(ngx_http_request_t *r);
static ngx_int_t ngx_http_memory_redirect_build_redirect_header(ngx_http_request_t *r, char* location, int location_len);
static ngx_int_t ngx_http_memory_redirect_handler(ngx_http_request_t *r);
static ngx_int_t ngx_http_memory_redirect_init(ngx_conf_t *cf);
static void * ngx_http_memory_redirect_create_loc_conf(ngx_conf_t *cf);
static char * ngx_http_memory_redirect_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child);

static ngx_http_memory_redirect_address_node ngx_http_memory_redirect_address_node_generator(char *str); //This function is to read csv file and save to ad.

// read info from csv file and save to this array.

static ngx_http_memory_redirect_bst_node *ngx_http_memory_redirect_root = NULL; // start node for b-tree.

static int ngx_http_memory_redirect_load_redict_csv_file(char *path, ngx_conf_t *cf); // this function loads the csv file.

static int ngx_http_memory_redirect_cmp_str(const char *a, const char *b); // this function compare two string and return -1, 0, ...

static void ngx_http_memory_redirect_insert(ngx_http_memory_redirect_address_node key, ngx_http_memory_redirect_bst_node** leaf, ngx_conf_t *cf); //b-tree ngx_http_memory_redirect_insert function, the second parameter should be the address of the b-tree node pointer.

//static void ngx_http_memory_redirect_list_in_order(ngx_http_memory_redirect_bst_node *root); // show b-tree in order

static ngx_http_memory_redirect_bst_node_address_info ngx_http_memory_redirect_search(char* key, ngx_http_memory_redirect_bst_node* leaf); //b-tree ngx_http_memory_redirect_search function.



/**
 * This module provided directive: hello world.
 *
 */
static ngx_command_t ngx_http_memory_redirect_commands[] = {

		{ ngx_string("memory_btree"), /* directive */
		  NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1, /* location context and takes
                                            no arguments*/
		  ngx_http_memory_btree, /* configuration setup function */
		  0, /* No offset. Only one context is supported. */
		  offsetof(ngx_http_memory_redirect_loc_conf_t, file_path), /* No offset when storing the module configuration on struct. */
		  NULL},
        { ngx_string("memory_redirect"), /* directive */
          NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_FLAG|NGX_CONF_TAKE1, /* location context and takes
                                            no arguments*/
          ngx_conf_set_flag_slot, /* configuration setup function */
          NGX_HTTP_LOC_CONF_OFFSET, /* No offset. Only one context is supported. */
          offsetof(ngx_http_memory_redirect_loc_conf_t, enabled), /* No offset when storing the module configuration on struct. */
          NULL},
		ngx_null_command /* command termination */
};

/* The hello world string. */
//static u_char ngx_hello_world[] = HELLO_WORLD;

/* The module context. */
static ngx_http_module_t ngx_http_memory_redirect_module_ctx = {
		NULL, /* preconfiguration */
        ngx_http_memory_redirect_init, /* postconfiguration */

		NULL, /* create main configuration */
		NULL, /* init main configuration */

		NULL, /* create server configuration */
		NULL, /* merge server configuration */

        ngx_http_memory_redirect_create_loc_conf, /* create location configuration */
        ngx_http_memory_redirect_merge_loc_conf /* merge location configuration */
};

/* Module definition. */
ngx_module_t ngx_http_memory_redirect_module = {
		NGX_MODULE_V1,
		&ngx_http_memory_redirect_module_ctx, /* module context */
		ngx_http_memory_redirect_commands, /* module directives */
		NGX_HTTP_MODULE, /* module type */
		NULL, /* init master */
		NULL, /* init module */
		NULL, /* init process */
		NULL, /* init thread */
		NULL, /* exit thread */
		NULL, /* exit process */
		NULL, /* exit master */
		NGX_MODULE_V1_PADDING
};
static ngx_int_t ngx_http_memory_btree_handler(ngx_http_request_t *r){

	ngx_buf_t *b;
    ngx_chain_t out;
    u_char *result;
    //int str_len;
    // Set the Content-Type header.
    //ngx_http_memory_redirect_loc_conf_t* conf = ngx_http_get_module_loc_conf(r, ngx_http_memory_redirect_module);
    r->headers_out.content_type.len = sizeof("text/html") - 1;
    r->headers_out.content_type.data = (u_char *) "text/html";

    ngx_http_memory_redirect_address_node address_node;
    if(ngx_http_memory_redirect_root == NULL){
            strcpy(address_node.new_address,"-10");
            strcpy(address_node.old_address,"-10");
            strcpy(address_node.redirect_code,"-10");
    }
    //    address_node = ngx_http_memory_redirect_search("http://cagesworld.co.uk/",ngx_http_memory_redirect_root);
    //}else{
    //    strcpy(address_node.new_address,"-10");
    //    strcpy(address_node.old_address,"-10");
    //    strcpy(address_node.redirect_code,"-10");
    //}
    char * test;
    if(ngx_http_memory_redirect_cmp_str(address_node.new_address,"-10") == 0){
        asprintf(&test, "failed! ");
    }else{
        asprintf(&test, "done!   ");
    }
    //str_len = strlen(address_node.new_address);


    result = (u_char *) test;
    // Allocate a new buffer for sending out the reply.
    b = ngx_pcalloc(r->pool, sizeof(ngx_buf_t));

    //Insertion in the buffer chain.
    out.buf = b;
    out.next = NULL; // just one buffer

    b->pos = result; // first position in memory of the data
    b->last = result + sizeof(result); // last position in memory of the data
    b->memory = 1; // content is in read-only memory
    b->last_buf = 1; // there will be no more buffers in the request

    // Sending the headers for the reply.
    r->headers_out.status = NGX_HTTP_OK; // 200 status code
    // Get the content length of the body.
    r->headers_out.content_length_n = sizeof(result);
    ngx_http_send_header(r); // Send the headers
    // Send the body, and return the status code of the output filter chain.
    return ngx_http_output_filter(r, &out);

	//ngx_http_send_header(r); // Send the headers
	//return NGX_OK;
}

static ngx_int_t ngx_http_memory_redirect_build_redirect_header(ngx_http_request_t *r, char* location, int location_len)
{
    ngx_http_clear_location(r);
    r->headers_out.location = ngx_list_push(&r->headers_out.headers);
    if (!r->headers_out.location) {
        printf("not sucess!");
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    // sending the header
    r->headers_out.status = NGX_HTTP_MOVED_PERMANENTLY;
    r->headers_out.content_length_n = 0;
    r->headers_out.location->hash = 1;
    r->headers_out.location->key.data = (u_char*) "Location";
    r->headers_out.location->key.len = sizeof("Location") - 1;
    r->headers_out.location->value.data = (u_char*) location;
    r->headers_out.location->value.len = location_len;
    r->header_only = 1;

    return NGX_OK;
}


/**
 * Content handler.
 *
 * @param r
 *   Pointer to the request structure. See http_request.h.
 * @return
 *   The status of the response generation.
 */
static ngx_int_t ngx_http_memory_redirect_handler(ngx_http_request_t *r)
{
    char* url;
    char* schema = "http";
    ngx_http_memory_redirect_loc_conf_t* conf = ngx_http_get_module_loc_conf(r, ngx_http_memory_redirect_module);

    if (!conf->enabled) {
        return NGX_DECLINED;
    }

#if (NGX_HTTP_SSL)
    if (r->connection->ssl) {
      schema = "https";
    }
#endif

    asprintf(&url, "%s://%.*s%.*s",
             schema,
             (int) r->headers_in.host->value.len, r->headers_in.host->value.data,
             (int) (r->uri_end - r->uri_start), r->uri_start
    );


    if (ngx_http_memory_redirect_root == NULL) {
		ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Btree is not ready for %s", url);
		ngx_http_send_header(r);
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    ngx_http_memory_redirect_bst_node_address_info reply = ngx_http_memory_redirect_search(url,ngx_http_memory_redirect_root);

    if (ngx_http_memory_redirect_cmp_str(reply.new_address,"-1") !=0) {
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Redirecting to [%s].", reply.new_address);

        int address_len = strlen(reply.new_address);

        if (ngx_http_memory_redirect_build_redirect_header(r, reply.new_address, address_len) != NGX_OK) {
            return NGX_HTTP_INTERNAL_SERVER_ERROR;
        }
        ngx_http_send_header(r);
        ngx_http_finalize_request(r, NGX_DONE);
        free(url);
        return NGX_DONE;

    }else{
        ngx_log_error(NGX_LOG_ERR, r->connection->log, 0, "Direct to [%s].", url);
        /*
        int address_len = strlen(url);
        if (ngx_http_memory_redirect_build_redirect_header(r, url, address_len) != NGX_OK) {
            return NGX_HTTP_INTERNAL_SERVER_ERROR;
        }
         */
        //ngx_http_send_header(r);
		//ngx_http_finalize_request(r, NGX_DONE);
    }
    //free(reply);
    free(url);
    return NGX_OK;

} /* ngx_http_memory_redirect_handler */

/**
 * Configuration setup function that installs the content handler.
 *
 * @param cf
 *   Module configuration structure pointer.
 * @param cmd
 *   Module directives structure pointer.
 * @param conf
 *   Module configuration structure pointer.
 * @return string
 *   Status of the configuration setup.
 */
static char *ngx_http_memory_btree(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
	ngx_http_core_loc_conf_t *clcf; /* pointer to core location configuration */
    ngx_str_t *value;
    value = cf->args->elts;
    //ngx_http_handler_pt *log_handler;
    char * file_name;

    file_name = (char *) value[1].data;
    /* Check and create b-tree. */
    //if(ngx_http_memory_redirect_root == NULL){
    int exist = cfileexists(file_name);
    if(strlen(file_name) != 0 && exist == 1){
        ngx_http_memory_redirect_load_redict_csv_file(file_name,cf);
    }else{
        //ngx_http_memory_redirect_load_redict_csv_file("/Users/jzhang/Sites/redirect_module/redirect.csv",cf);
        //return NGX_CONF_ERROR;
        printf("No redirect CSV file found - please check location that the file exists: %s\n", file_name);
        exit(1);
    }

    //}

    //ngx_http_memory_redirect_load_redict_csv_file(file_name,cf);

	/* Install the betree handler. */
	clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
    //ngx_log_error(NGX_LOG_WARN, r->connection->log, 0,
    //              "The csv file is \"%s\" ", file_name);

	clcf->handler = ngx_http_memory_btree_handler;

	return NGX_CONF_OK;
}
/*
 *  Check the input csv file exist or not
 */
static int cfileexists(const char* filename){
    struct stat buffer;
    int exist = stat(filename,&buffer);
    if(exist == 0)
        return 1;
    else // -1
        return 0;
}
/*
static char *ngx_http_memory_redirect(ngx_conf_t *cf, ngx_command_t *cmd, void *conf){
    ngx_http_core_loc_conf_t *clcf; // pointer to core location configuration

    // Install the redirect handler.
    clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);

    clcf->handler = ngx_http_memory_redirect_handler;

    return NGX_CONF_OK;
}
*/
/* ngx_http_memory_redirect */


static int ngx_http_memory_redirect_load_redict_csv_file(char *path, ngx_conf_t *cf){
    //system("ulimit -s hard");
	FILE* stream = fopen (path,"r");
	char line[1670];
	//ngx_http_memory_redirect_address_node *ad = malloc(15000 * sizeof(ngx_http_memory_redirect_address_node));
	int i=0;//ad_size=0;
	while (fgets(line,1670,stream)){
		/*printf("%s \n",line);*/
		//char temp_line[1670];
		//strcpy(temp_line,line);
		ngx_http_memory_redirect_address_node temp_ad;
        temp_ad = ngx_http_memory_redirect_address_node_generator(line);
        ngx_http_memory_redirect_insert(temp_ad, &ngx_http_memory_redirect_root, cf);
        i++;
	}
	/*
    ad_size = i;
	if(ad_size>0){
		for(i=0;i<ad_size;i++){
			//printf("%s \n",ad[i].old_address);
			ngx_http_memory_redirect_insert(ad[i], &ngx_http_memory_redirect_root, cf);
		}
	}
	free(ad);
	 */
	return i;
}

static void ngx_http_memory_redirect_insert(ngx_http_memory_redirect_address_node key, ngx_http_memory_redirect_bst_node** leaf, ngx_conf_t *cf)
{
	int res;
	if( *leaf == NULL ) {
		//*leaf = (ngx_http_memory_redirect_bst_node*) malloc( sizeof( struct ngx_http_memory_redirect_bst_node ) );
		*leaf = ngx_pcalloc(cf->pool, sizeof( struct ngx_http_memory_redirect_bst_node ));
        //ngx_http_memory_redirect_bst_node_address_info temp_address_info;
        strcpy((*leaf)->address_info.new_address,key.new_address);
        strcpy((*leaf)->address_info.redirect_code,key.redirect_code);
		//(*leaf)->address_info = temp_address_info;                   // copy the key
		strcpy((*leaf)->index, key.old_address);
		(*leaf)->lchild = NULL;
		(*leaf)->rchild = NULL;
	} else {
		res = ngx_http_memory_redirect_cmp_str (key.old_address, (*leaf)->index);
		if( res < 0)
			ngx_http_memory_redirect_insert( key, &(*leaf)->lchild, cf);
		else if( res > 0)
			ngx_http_memory_redirect_insert( key, &(*leaf)->rchild, cf);
		else                                            // key already exists
			//printf ("Key '%s' already in tree\n", key.old_address);
            //ngx_http_memory_redirect_bst_node_address_info temp_address_info;
            strcpy((*leaf)->address_info.new_address,key.new_address);
            strcpy((*leaf)->address_info.redirect_code,key.redirect_code);
            //(*leaf)->address_info = temp_address_info;
	}
}

static int ngx_http_memory_redirect_cmp_str(const char *a, const char *b)
{
	return (strcmp (a, b));     // string comparison instead of pointer comparison
}
/*
static void ngx_http_memory_redirect_list_in_order(ngx_http_memory_redirect_bst_node *root)
{
	if( root != NULL ) {
		ngx_http_memory_redirect_list_in_order(root->lchild);
		printf("   %s\n", root->index);     // string type
		printf("   %s\n", root->address_info.new_address);     // string type
		printf("   %s\n", root->address_info.redirect_code);     // string type
		printf("\n");     // string type
		ngx_http_memory_redirect_list_in_order(root->rchild);
	}
}
*/
static ngx_http_memory_redirect_bst_node_address_info ngx_http_memory_redirect_search(char* key, ngx_http_memory_redirect_bst_node* leaf)  // no need for **
{
	int res;
	if( leaf != NULL ) {
		res = ngx_http_memory_redirect_cmp_str(key, leaf->index);
		if( res < 0)
			return ngx_http_memory_redirect_search( key, leaf->lchild);
		else if( res > 0)
			return ngx_http_memory_redirect_search( key, leaf->rchild);
		else
			return leaf->address_info;     // string type
	}
	else {
        ngx_http_memory_redirect_bst_node_address_info nothing;
		strcpy(nothing.new_address,"-1");
		strcpy(nothing.redirect_code,"-1");
		return nothing;
	}
}

static ngx_http_memory_redirect_address_node ngx_http_memory_redirect_address_node_generator(char *str){
	ngx_http_memory_redirect_address_node return_node;
	int len;
	len = strlen(str);
	strcpy(return_node.old_address,strtok(str,","));

	int old_address = strlen(return_node.old_address) + 1;
	char* str_rest;
	str_rest = strndup(str + old_address, len);
	strcpy(return_node.new_address,strtok(str_rest,","));

	int new_address = strlen(return_node.new_address) + 1;
	strcpy(return_node.redirect_code,strndup(str_rest + new_address, 3));

	return return_node;
}


static ngx_int_t ngx_http_memory_redirect_init(ngx_conf_t *cf)
{
    ngx_http_handler_pt        *h;
    ngx_http_core_main_conf_t  *cmcf;

    cmcf = ngx_http_conf_get_module_main_conf(cf, ngx_http_core_module);

    h = ngx_array_push(&cmcf->phases[NGX_HTTP_ACCESS_PHASE].handlers);
    if (!h) {
        return NGX_ERROR;
    }

    *h = ngx_http_memory_redirect_handler;

    return NGX_OK;
}

static void * ngx_http_memory_redirect_create_loc_conf(ngx_conf_t *cf)
{
    ngx_http_memory_redirect_loc_conf_t  *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_memory_redirect_loc_conf_t));
    if (!conf) {
        return NGX_CONF_ERROR;
    }
    conf->enabled = NGX_CONF_UNSET;
    return conf;
}

// merge location config
static char * ngx_http_memory_redirect_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_http_memory_redirect_loc_conf_t *prev = parent;
    ngx_http_memory_redirect_loc_conf_t *conf = child;

    ngx_conf_merge_value(conf->enabled, prev->enabled, 0);
    /*
    ngx_conf_merge_str_value(conf->redis_hostname, prev->redis_hostname, "localhost");
    ngx_conf_merge_uint_value(conf->redis_port, prev->redis_port, 6379);
    ngx_conf_merge_uint_value(conf->redis_db, prev->redis_db, 0);
     */
    return NGX_CONF_OK;
}
